import { Component, OnInit  } from '@angular/core';
import {AuctionService} from './auction.service'
import {UserService} from './user.service'

import { Observable, Subscription } from 'rxjs';
import {Auction} from '../models/Auction'
import {Response} from '../models/Response'
import {User} from '../models/User';
import { Socket } from 'ngx-socket-io';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angChl';
  
  auctions: Auction[] = [];
  response: Response[];
  users: User[];
  infos ;
  counter: Observable<number>;
  subscription: Subscription
  constructor(
    private auctionService: AuctionService,
    private userService: UserService,
    private socket: Socket
    ){

  }

 

  async ngOnInit(){
    
    this.auctionService.getBroadcast().subscribe((i:any)=> {
      console.log(i)
      this.infos = i
    })
    await this.load()

  }
  async load(){
    await this.getAuctions()
    await this.getUsers()
  }

  

  async getAuctions() {
    this.auctions = await this.auctionService.getAuctions()
  }

  async getUsers(){
    this.users = await this.userService.getUsers()
  }

  async reload(){
    this.socket.disconnect()
    await this.load()
    console.log('reloading..')
    this.socket.connect()

  }

  ngOnDestroy(): void {
    this.socket.disconnect()
}

  placeBid(auctionId,userId,bid){
    
    this.auctionService.placeBid(auctionId,userId,bid.value)
    bid.value = null
  }

  
  
}
