import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuctionService {
  url = 'http://localhost:3000'
  infos = this.socket.fromEvent('broadcast');

  constructor(
    private socket: Socket,
    private http: HttpClient

  ) { 
    
  }

  ping(){
    this.socket.emit('try',{ping:'ping'})
    
  }

  getBroadcast(){
    let observable = new Observable(observer => {


      this.socket.on('broadcast', (data) => {

        observer.next(data);

      });


    })

    return observable;
  }

 

  placeBid(auctionId,userId,amount){
    this.socket.emit('placeBid',{auctionId,userId,amount:parseInt(amount)})
  }


  async getAuctions() {
    const response:any = await this.http.get(`${this.url}/auctions`).toPromise()
    return response.data
  }
}
