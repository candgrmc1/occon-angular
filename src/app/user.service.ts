import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = 'http://localhost:3000'

  constructor(
    private http: HttpClient
  ) { }

  async getUsers(){
    const response:any = await this.http.get(`${this.url}/users`).toPromise()
    console.log(response)
    return response.data
  }
}
