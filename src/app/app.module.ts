import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { HttpClientModule } from '@angular/common/http';
import { AuctionService } from './auction.service'
import {UserService} from './user.service'
const config: SocketIoConfig = { url: 'http://localhost:8080', options: {} };

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SocketIoModule.forRoot(config),
    HttpClientModule
  ],
  providers: [AuctionService,UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
