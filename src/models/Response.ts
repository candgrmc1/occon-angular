export interface Response {
    success: String;
    data: Array<any>;
}