export interface Auction {
    _id: string;
    name: string;
    basePrice: number;
    createdAt: Date;
    deadLine: Date
}